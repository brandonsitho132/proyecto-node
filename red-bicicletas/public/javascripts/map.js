var mymap = L.map('main_map').setView([10.933412, -74.832905], 13);
//agregamos el mapa
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
}).addTo(mymap);
L.marker([10.933412, -74.832905]).addTo(mymap);

//utilizamos la api para agregar bicicletas al mapa en la base de datos
$.ajax({
    dataType: "json",
    //aqui buscamos la api
    url: "api/bicicletas",
    success: function(result) {
    	//imprimimos el resultado
        console.log(result);
        //ahora el resultado lo ponemos a escuchar y agregamos el marcador
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion,{title:bici.id} ).addTo(mymap);
        });
    }
})