var bicicleta = require('../../models/bicicleta');
//creamos una lista de bicicletas
exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: bicicleta.allbicis
    });
}
//creamos bicicletas
exports.bicicleta_create = function(req, res) {
    //tomamos cada id de la pagina y lo guardamos en la clase bici
    var bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    //cogemos las variables latitud y ubicacion y lo guardamos en la cadena ubicacion
    bici.ubicacion = [req.body.latitud, req.body.longitud];
    //agregamos la bicicletas a la clase
    bicicleta.add(bici);
    //ledamos una respuesta a esto
    res.status(200).json({
    	bicicleta: bici
    });
}

exports.bicicleta_delete= function(req,res){
	bicicleta.removeById(req.body.id);
	res.status(204).send();
}