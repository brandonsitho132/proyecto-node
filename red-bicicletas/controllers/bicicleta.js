var bicicleta = require('../models/bicicleta');
exports.bicicleta_list = function(req, res) {
    res.render('bicicleta/index', {
        bicis: bicicleta.allbicis
    });
}
exports.bicicleta_create_get = function(req, res) {
    res.render('bicicleta/create');
}
exports.bicicleta_create_post = function(req, res) {
    var bicis = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bicis.ubicacion = [req.body.latitud, req.body.longitud];
    bicicleta.add(bicis);
    res.redirect('/bicicletas');
}
exports.bicicleta_update_get = function(req, res) {
    var bici = bicicleta.findById(req.params.id);
    res.render('bicicleta/update', {bici});
}
exports.bicicleta_update_post = function(req, res) {
    var bici = bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.latitud, req.body.longitud];
    res.redirect('/bicicletas');
}
exports.bicicleta_delete_post = function(req, res) {
    bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}